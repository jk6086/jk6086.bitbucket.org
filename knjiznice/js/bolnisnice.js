var mapa;

var bolnisnice = [];

var marker;

window.addEventListener('load', function () {
	var options = {
		center: [46.05004, 14.46931],
		zoom: 12
	};
	
	var ikona = new L.Icon({
	    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
	      'marker-icon-2x-red.png',
	    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
	      'marker-shadow.png',
	    iconSize: [25, 41],
	    iconAnchor: [12, 41],
	    popupAnchor: [1, -34],
	    shadowSize: [41, 41]
	  });
	
	marker = L.marker([46.05004, 14.46931], {icon: ikona});

	/* global L, distance */
	mapa = new L.map("mapa", options);

	var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

	mapa.addLayer(layer);

	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
	  if (this.readyState == 4 && this.status == 200) {
	    var json = JSON.parse(this.responseText);

	    for (var a in json.features) {
	    	a = json.features[a];
	    	var obj = {
	    		ime: (a.properties.website != undefined ?
	    		("<a href="+a.properties.website+" target=\"_blank\">" + a.properties.name + "</a>") :
	    		a.properties.name),
	    		kraj: (a.properties["addr:street"] != undefined ? ("<br>" + a.properties["addr:street"] + " " +
	    			(a.properties["addr:housenumber"] != undefined ? a.properties["addr:housenumber"] : "")) : "") +
	    			(a.properties["addr:city"] != undefined ? ("<br>" + a.properties["addr:city"]) : ""),
	    		polygon: null
	    	};

	    	if (a.geometry.type === "Polygon") {
				obj = makePolygon(obj, a.geometry);

	    		bolnisnice.push(obj);
	    	}
	    }
	    
	    mapa.on("click", onClick);
	  }
	};
	xmlhttp.open("GET", "knjiznice/json/bolnisnice.json", true);
	xmlhttp.send();
});

function makePolygon(obj, geometry) {
	for (var i = 0; i < geometry.coordinates.length; i++)
		for (var j = 0; j < geometry.coordinates[i].length; j++) {
			var b = geometry.coordinates[i][j][0];
			geometry.coordinates[i][j][0] = geometry.coordinates[i][j][1];
			geometry.coordinates[i][j][1] = b;
		}

	obj.polygon = L.polygon(
		geometry.coordinates,
		{color: 'blue'}).addTo(mapa);

	obj.center = obj.polygon.getCenter();
	
	if (obj.ime != undefined)
		obj.polygon.bindPopup(obj.ime + obj.kraj);
		
	return obj;
}

function onClick(lokacija) {
	var latLng = lokacija.latlng;
	for (var i = 0; i < bolnisnice.length; i++) {
		if (distance(latLng.lat, latLng.lng,
			bolnisnice[i].center.lat, bolnisnice[i].center.lng,
			"K") < 5) {
			bolnisnice[i].polygon.setStyle({color: 'green'});
		} else {
			bolnisnice[i].polygon.setStyle({color: 'blue'});
		}
	}
	
	marker.setLatLng(latLng);
	marker.addTo(mapa);
}