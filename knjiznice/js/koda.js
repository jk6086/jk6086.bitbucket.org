/* global $, btoa, d3 */

var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var siStatUrl = "https://pxweb.stat.si:443/SiStatDb/api/v1/sl/10_Dem_soc/08_zivljenjska_raven/17_silc_zdravje/05_08685_splosno_zdravst_stanje/0868515S.px";
var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  var ime = "";
  var priimek = "";
  var rojstvo = "";
  var spol = "";
  var visina = "";
  var meritve = [];
  
  switch (stPacienta) {
    case 1:
      ime = "Edward";
      priimek = "Caraway";
      rojstvo = "1969-06-23";
      spol = "MALE";
      meritve = [
        ["2013-01-03", "174.3", "82.4", "36.7", "111", "76", "97"],
        ["2014-05-15", "174.8", "83.2", "36.9", "113", "77", "96"],
        ["2016-08-12", "176.6", "85.7", "36.8", "112", "78", "94"],
        ["2017-11-23", "176.9", "86.0", "36.8", "114", "75", "98"]
      ];
      break;
    case 2:
      ime = "Bonita";
      priimek = "Schaffer";
      rojstvo = "1991-05-09";
      spol = "FEMALE";
      meritve = [
        ["2016-04-12", "167.1", "50.4", "35.8", "140", "93", "96"],
        ["2017-05-21", "168.4", "51.2", "35.9", "142", "90", "95"],
        ["2017-06-23", "168.6", "51.4", "36.2", "145", "91", "94"],
        ["2017-09-01", "169.0", "51.7", "35.9", "147", "90", "95"]
      ];
      break;
    case 3:
      ime = "Jose";
      priimek = "Adcock";
      rojstvo = "1956-09-09";
      spol = "MALE";
      meritve = [
        ["2012-06-30", "188.0", "86.3", "36.0", "125", "73", "95"],
        ["2013-08-20", "188.0", "86.2", "36.1", "130", "74", "97"],
        ["2014-11-14", "188.0", "86.0", "36.2", "128", "75", "98"],
        ["2016-12-24", "188.1", "86.1", "35.9", "129", "79", "98"]
      ];
      break;
    default:
      return undefined;
  }
  
  var ehrId = undefined;
  
  $.ajax(baseUrl + "/ehr", { // generiranje novega ehrId
    type: "POST",
    headers: {Authorization: getAuthorization()},
    async: false, // zato da se nastavi ehrId
    error: function (napaka) {
      console.log("1 " + JSON.parse(napaka.responseText).userMessage);
    },
    success: function (data) {
      ehrId = data.ehrId;
      
      var podatki = {
        firstNames: ime,
        lastNames: priimek,
        dateOfBirth: rojstvo,
        gender: spol,
        additionalInfo: {"ehrId": ehrId}
      };
      
      $.ajax(baseUrl + "/demographics/party", {
        type: "POST",
        headers: {Authorization: getAuthorization()},
        contentType: "application/json",
        data: JSON.stringify(podatki),
        async: false,
        error: function (napaka) {
          console.log("2 " + JSON.parse(napaka.responseText).userMessage);
        },
        success: function (party) {
          if (party.action != "CREATE") {
            console.log("3");
            console.log(party);
            return;
          }
          
          $("#infoVzorcni").css('display','none');
          $("#izbiraVzorcni").append("<option value=" + ehrId + ">" +podatki.firstNames +
            " " + podatki.lastNames + " [" + podatki.dateOfBirth + "]</option>");
          $("#izbiraVzorcni").css('display','initial');
              
          
          for (var i = 0; i < meritve.length; i++) {
            shraniMeritve(ehrId,
              meritve[i][0],
              meritve[i][1],
              meritve[i][2],
              meritve[i][3],
              meritve[i][4],
              meritve[i][5],
              meritve[i][6]);
          }
        }
      });
    }
  });
  
  return ehrId;
}

function dobiJSON(url, errId, callback) {
  $.ajax(baseUrl + url, {
    type: "GET",
    headers: {Authorization: getAuthorization()},
    async: false,
    error: function (napaka) {
      console.log(errId + " " + JSON.parse(napaka.responseText).userMessage);
    },
    success: callback
  });
}

function shraniMeritve(ehrId, datum, visina, teza, temperatura, stlak, dtlak, kisik) {
  var podatki = {
    "ctx/language": "en",
    "ctx/territory": "SI",
    "ctx/time": datum,
    "vital_signs/height_length/any_event/body_height_length": visina,
    "vital_signs/body_weight/any_event/body_weight": teza,
   	"vital_signs/body_temperature/any_event/temperature|magnitude": temperatura,
    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
    "vital_signs/blood_pressure/any_event/systolic": stlak,
    "vital_signs/blood_pressure/any_event/diastolic": dtlak,
    "vital_signs/indirect_oximetry:0/spo2|numerator": kisik
  };
  
  var parametri = {
    ehrId: ehrId,
    templateId: 'Vital Signs',
    format: 'FLAT'
  };
  
  $.ajax(baseUrl + "/composition?" + $.param(parametri), {
    type: "POST",
    contentType: "application/json",
    data: JSON.stringify(podatki),
    async: false,
    headers: {Authorization: getAuthorization()},
    error: function (napaka) {
      console.log("4 " + JSON.parse(napaka.responseText).userMessage);
    }
  });
}

function generiraj() {
  var podatki = [];
  for (var i = 0; i < 3; i++) podatki.push(generirajPodatke(i+1));
  return podatki;
}

function dobiPodatke(ehrId) {
  var podatki = {};
  
  dobiJSON("/demographics/ehr/" + ehrId + "/party", 5, function (party) {
    podatki.party = party.party;
    
    dobiJSON("/view/" + ehrId + "/height", 6, function (height) {
      dobiJSON("/view/" + ehrId + "/weight", 7, function (weight) {
        dobiJSON("/view/" + ehrId + "/blood_pressure", 8, function (blood_pressure) {
          podatki.data = [];
          for (var i = 0; i < height.length; i++) {
            var zdravje = 5;
            
            var bmi = (weight[i].weight)/(Number(height[i].height) * Number(height[i].height) / 10000);
            
            if (bmi < 15) zdravje -= 2; // hudo podhranjen
            else if (bmi < 18) zdravje--; // podhranjen
            else if (bmi > 40) zdravje -= 2; // hudo prenahranjen
            else if (bmi > 30) zdravje--; // prenahranjen
            
            var s = blood_pressure[i].systolic;
            var d = blood_pressure[i].diastolic;
            
            if (s > 180 || d > 120) zdravje -= 5; // takoj na zdravljenje
            else if (s > 140 || d > 90) zdravje -= 3;
            else if (s > 130 || d > 80) zdravje -= 2;
            else if (s > 120 && d < 80) zdravje--;
            
            if (zdravje < 1) zdravje = 1; 
            
            podatki.data.unshift({
              leto: height[i].time.substring(0,4),
              datum: height[i].time,
              bmi: bmi,
              systolic: s,
              diastolic: d,
              zdravje: zdravje
            });
          }
        });
      });
    });
  });
  
  return podatki;
}

function izbiraVzorca() {
  var ehrId = $("#izbiraVzorcni").val();
  
  var podatki = dobiPodatke(ehrId);
  
  vizualizacija(podatki);
}

function ehrInput(ele) {
  if (event.key === "Enter") {
    var ehrId = ele.value;

    var podatki = dobiPodatke(ehrId);

    vizualizacija(podatki);
  }
}


function primerjajZUradom(podatek, podatki) {
  var formatirano = {};
  
  // statisticni urad RS
  $.ajax(siStatUrl, {
    type: "POST",
    contentType: "application/json",
    data: JSON.stringify({
      "query": [{"code": "SPOL",
        "selection": {
          "filter": "item", "values": [ podatki.party.spol == "MALE" ? "1" : "2" ]
        }
      },{"code": "LETO",
        "selection": {
          "filter": "item", "values": [ podatek.leto ]
        }
      }],
      "response": {
        "format": "json-stat"
      }
    }),
    async: false,
    error: function (napaka) {
      console.log("9 " + napaka.responseText);
    },
    success: function (siStat) {
      $("#primerjavaPie").html("");
      
      $("#primerjavaSpol").html(podatki.party.spol == "MALE" ? "moški" : "ženski");
      $("#primerjavaLeto").html(podatek.leto);
      
      $("#primerjavaRow").css('display','initial');
      
      var width = 450;
      var height = 450;
      var margin = 40;
      
      var radius = Math.min(width, height) / 2 - margin;
      
      var svg = d3.select("#primerjavaPie")
        .append("svg")
          .attr("width", width)
          .attr("height", height)
        .append("g")
          .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
          
      // set the color scale
      var color = d3.scaleOrdinal()
        .domain(siStat.dataset.value)
        .range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56"])
        
      // Compute the position of each group on the pie:
      var pie = d3.pie()
        .value(function(d) {return d.value; })
      var data_ready = pie(d3.entries(siStat.dataset.value));
      
      var i = 0;
      
      // shape helper to build arcs:
      var arcGenerator = d3.arc()
        .innerRadius(function(d) {
            if (++i + podatek.zdravje == 6) return radius/4;
            return radius/2;
        })
        .outerRadius(radius);
      
      
      
      // Build the pie chart: Basically, each part of the pie is a path that we build using the arc function.
      svg
        .selectAll('whatever')
        .data(data_ready)
        .enter()
        .append('path')
        .attr('d', arcGenerator)
        .attr('fill', function(d){ return(color(d.data.key)) })
        .attr("stroke", "black")
        .style("stroke-width", "2px")
        .style("opacity", 0.7)
      
      svg
        .selectAll('mySlices')
        .data(data_ready)
        .enter()
        .append('text')
        .text(function(d){
          console.log(siStat.dataset.dimension["ZDRAVSTVENO STANJE"].category.label);
          return siStat.dataset.dimension["ZDRAVSTVENO STANJE"].category.label[Number(d.data.key)+1];
        })
        .attr("transform", function(d) { return "translate(" + arcGenerator.centroid(d) + ")";  })
        .style("text-anchor", "middle")
        .style("font-size", 17)
    }
  });
  
  return formatirano;
}

function vizualizacija(podatki) {
  $("#primerjavaPie").html("");
  $("#bmiChart").html("");
  $("#tlakChart").html("");
  
  $("#bmiInfo").css('display','initial');
  $("#tlakInfo").css('display','initial');
  $("#primerjavaRow").css('display','none');
  
  var margin = {top:30, right:30, bottom:30, left:30};
  var width = 600;
  var height = 600;
  
  var mouseClick = function(d){
    //console.log(d);
    primerjajZUradom(d, podatki);
  };
  
  var svg;
  
  svg = d3.select("#bmiChart").append("svg")
    .attr("width", width+margin.left+margin.right)
    .attr("height", height+margin.top+margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.right + ")");
  
  var max = podatki.data[0].bmi;
  var min = podatki.data[0].bmi;
  
  for (var i = 0; i < podatki.data.length; i++) {
    if (podatki.data[i].bmi > max) max = podatki.data[i].bmi;
    if (podatki.data[i].bmi < min) min = podatki.data[i].bmi;
  }
  
  //console.log([Number(podatki.data[0].leto),Number(podatki.data[podatki.data.length - 1].leto)]);
  
  var x = d3.scaleTime()
        .domain([new Date(Number(podatki.data[0].leto),0,1),
                 new Date(Number(podatki.data[podatki.data.length - 1].leto) + 1,0,1)
        ])
        .range([0,width]);
  var y = d3.scaleLinear()
        .domain([min-20,max+20])
        .range([height,0]);
        
  svg.append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x));
  svg.append("g")
      .call(d3.axisLeft(y));
      
  svg.append("path")
    .datum(podatki.data)
    .attr("fill", "none")
    .attr("stroke", "black")
    .attr("stroke-width", 2)
    .attr("d", d3.line()
      .x(function(d) { return x(new Date(d.datum)); })
      .y(function(d) { return y(d.bmi); })
      );
      
  svg
    .append("g")
    .selectAll("dot")
    .data(podatki.data)
    .enter()
    .append("circle")
      .attr("cx", function(d) { return x(new Date(d.datum)); } )
      .attr("cy", function(d) { return y(d.bmi); } )
      .attr("r", 7)
      .attr("fill", function(d) {
        if (d.bmi < 15) return "red";
        if (d.bmi < 18) return "orange";
        if (d.bmi > 50) return "purple";
        if (d.bmi > 40) return "red";
        if (d.bmi > 30) return "orange";
        if (d.bmi > 25) return "yellow";
        return "green";
      })
      .on("click", mouseClick);
      
  // ------------- ponovno za tlak
  
  svg = d3.select("#tlakChart").append("svg")
    .attr("width", width+margin.left+margin.right)
    .attr("height", height+margin.top+margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.right + ")");
  
  max = podatki.data[0].systolic;
  min = podatki.data[0].systolic;
  
  for (var i = 0; i < podatki.data.length; i++) {
    if (podatki.data[i].systolic > max) max = podatki.data[i].systolic;
    if (podatki.data[i].diastolic > max) max = podatki.data[i].diastolic;
    if (podatki.data[i].systolic < min) min = podatki.data[i].systolic;
    if (podatki.data[i].diastolic < min) min = podatki.data[i].diastolic;
  }
  
  //console.log([Number(podatki.data[0].leto),Number(podatki.data[podatki.data.length - 1].leto)]);
  
  x = d3.scaleTime()
        .domain([new Date(Number(podatki.data[0].leto),0,1),
                 new Date(Number(podatki.data[podatki.data.length - 1].leto) + 1,0,1)
        ])
        .range([0,width]);
  y = d3.scaleLinear()
        .domain([min-20,max+20])
        .range([height,0]);
        
  svg.append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x));
  svg.append("g")
      .call(d3.axisLeft(y));
      
  svg.append("path")
    .datum(podatki.data)
    .attr("fill", "none")
    .attr("stroke", "red")
    .attr("stroke-width", 1.5)
    .attr("d", d3.line()
      .x(function(d) { return x(new Date(d.datum)); })
      .y(function(d) { return y(d.systolic); })
      );
      
  svg
    .append("g")
    .selectAll("dot")
    .data(podatki.data)
    .enter()
    .append("circle")
      .attr("cx", function(d) { return x(new Date(d.datum)); } )
      .attr("cy", function(d) { return y(d.systolic); } )
      .attr("r", 7)
      .attr("fill", function(d) {
        if (d.systolic < 120) return "green";
        if (d.systolic < 130) return "yellow";
        if (d.systolic < 140) return "orange";
        if (d.systolic < 180) return "red";
        return "purple";
      })
      .on("click", mouseClick);
      
  svg.append("path")
    .datum(podatki.data)
    .attr("fill", "none")
    .attr("stroke", "blue")
    .attr("stroke-width", 1.5)
    .attr("d", d3.line()
      .x(function(d) { return x(new Date(d.datum)); })
      .y(function(d) { return y(d.diastolic); })
      );
      
  svg
    .append("g")
    .selectAll("dot")
    .data(podatki.data)
    .enter()
    .append("circle")
      .attr("cx", function(d) { return x(new Date(d.datum)); } )
      .attr("cy", function(d) { return y(d.diastolic); } )
      .attr("r", 7)
      .attr("fill", function(d) {
        if (d.diastolic < 80) return "green";
        if (d.diastolic < 90) return "orange";
        if (d.diastolic < 120) return "red";
        return "purple";
      })
      .on("click", mouseClick);
}

window.addEventListener("load", function() {
  document.getElementById("generiranje").addEventListener("click",function(){
    generiraj();
  });
});